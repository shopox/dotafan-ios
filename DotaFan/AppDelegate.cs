﻿using System;
using System.Collections.Generic;
using System.Linq;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace DotaFan
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		
		public override UIWindow Window {
			get;
			set;
		}

        UITabBarController tabController;

        private void CreateControllers()
        {
            tabController = new UITabBarController();
            // tabs
            tabController.ViewControllers = new[]
            {
                new DFNavController(new MatchesController()),
                new DFNavController(new TournamentsController()),
                new DFNavController(new TeamsController()),
                new DFNavController(new MeController()),
            };

            int i = 0;
            foreach (var vc in tabController.ViewControllers)
            {
                vc.TabBarItem.Image = UIImage.FromFile(string.Format("Tabs/{0}.png", i));
                vc.TabBarItem.SelectedImage = UIImage.FromFile(string.Format("Tabs/{0}a.png", i));
                i++;
            }

            if (tabController.TabBar.RespondsToSelector(new MonoTouch.ObjCRuntime.Selector("setTranslucent:")))
                tabController.TabBar.Translucent = false;

            // >= 7
            if (UIDevice.CurrentDevice.CheckSystemVersion(7, 0))
            {
                // ios 7 red appearance
                UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes { TextColor = UIColor.White });
                UINavigationBar.Appearance.BarTintColor = UIColor.FromRGB(0xb3, 0x1d, 0x1d);
                UINavigationBar.Appearance.TintColor = UIColor.White;
                // light status bar
                UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.LightContent;
                tabController.TabBar.TintColor = UIColor.FromRGB(0xb3, 0x1d, 0x1d);
            };
        }
		
		public override bool FinishedLaunching (UIApplication application, NSDictionary launchOptions)
		{
			Window = new UIWindow(UIScreen.MainScreen.Bounds);
            CreateControllers();

			Window.RootViewController = tabController;
			Window.MakeKeyAndVisible();

            var urlCache = new NSUrlCache(4 * 1024 * 1024, 20 * 1024 * 1024, "dotafan");
            NSUrlCache.SharedCache = urlCache;

			return true;
		}
	}
}

