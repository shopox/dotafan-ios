﻿using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Threading.Tasks;
using System.Linq;

namespace DotaFan
{
    public class DFListTableView<T, TCell> : DFTableView where TCell : DFListTableViewCell<T>, new()
    {
        public List<string> Sections { get; private set; }
        public Dictionary<string, List<T>> TableList { get; private set; }
        public int Count = 0;

        public event EventHandler LoadMore;
        public event EventHandler<RowClickedEventArgs<T>> RowClicked;
        public event EventHandler Refresh;
        public event EventHandler<SearchClickedEventArgs> SearchClicked;
        public bool HasMore { get; set; }
        public bool HasSearchBar
        {
            get
            {
                return searchBar != null;
            }
            set
            {
                if (value != HasSearchBar)
                {
                    if (value)
                    {
                        searchBar = new UISearchBar();
                        searchBar.Frame = new RectangleF(0, 0, Frame.Width, 40);
                        searchBar.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
                        searchBar.ShowsCancelButton = true;
                        /*if (searchBar.RespondsToSelector(new MonoTouch.ObjCRuntime.Selector("setBarTintColor:")))
                        {
                            // red + white
                            UIBarButtonItem.AppearanceWhenContainedIn(typeof(UISearchBar)).TintColor = UIColor.White;
                            searchBar.BarTintColor = UIColor.FromRGB(0xb3, 0x1d, 0x1d);
                        }*/
                        searchBar.SearchButtonClicked += (sender, e) => 
                        {
                            if (SearchClicked != null)
                                SearchClicked(this, new SearchClickedEventArgs { Text = searchBar.Text } );
                        };
                        searchBar.CancelButtonClicked += (sender, e) => 
                        {
                            searchBar.Text = "";

                            if (SearchClicked != null)
                                SearchClicked(this, new SearchClickedEventArgs { Text = searchBar.Text } );
                        };
                        TableHeaderView = searchBar;
                    }
                    else
                    {
                        TableHeaderView = null;
                        searchBar = null;
                    };
                };
            }
        }
        private UIRefreshControl rc;
        private UISearchBar searchBar;

        public DFListTableView()
        {
            Initialize();
        }

        public DFListTableView(RectangleF frame) : base(frame)
        {
            Initialize();
        }

        public DFListTableView(RectangleF frame, UITableViewStyle style) : base(frame, style)
        {
            Initialize();
        }       

        private void Initialize()
        {
            SeparatorStyle = UITableViewCellSeparatorStyle.SingleLine;

            TableList = new Dictionary<string, List<T>>();
            Sections = new List<string>();
            ClearList();
            HasMore = true;

            rc = new UIRefreshControl();
            rc.ValueChanged += (sender, e) =>
            {
                if (Refresh != null)
                    Refresh(this, EventArgs.Empty);
            };
            AddSubview(rc);

            DataSource = new ListDataSource(this);
            Delegate = new ListDelegate(this);
        }

        public void EndRefresh()
        {
            rc.EndRefreshing();
        }

        protected void DoLoadMore()
        {
            if (LoadMore != null)
                LoadMore(this, EventArgs.Empty);
        }

        protected void DoRowClicked(int section, int row)
        {
            if (RowClicked != null)
            {
                if (section >= 0 && section < Sections.Count)
                {
                    var sectionKey = Sections[section];
                    List<T> list;
                    if (TableList.TryGetValue(sectionKey, out list))
                    {
                        if (row >= 0 && row < list.Count)
                            RowClicked(this, new RowClickedEventArgs<T> { Object = list[row] });
                    }
                }
            }
        }

        private void ClearList()
        {
            TableList.Clear();
            Sections.Clear();
            Sections.Add("");
            TableList.Add("", new List<T>());
            Count = 0;
        }

        public void LoadData(Task<List<T>> task, bool Clear = true, Action<bool> callback = null)
        {
            LoadData(task.ContinueWith(t => 
            {
                return new LoadDataTaskResultDict<T> { Dict = t.Result.GroupBy(x => "").ToDictionary(x => x.Key, x => x.ToList())  };
            }), Clear, callback);
        }

        public void LoadData(Task<Dictionary<string, List<T>>> task, bool Clear = true, Action<bool> callback = null)
        {
            LoadData(task.ContinueWith(t =>
            {
                return new LoadDataTaskResultDict<T> { Dict = t.Result };
            }), Clear, callback);
        }

        public void LoadData(Task<LoadDataTaskResultList<T>> task, bool Clear = true, Action<bool> callback = null)
        {
            LoadData(task.ContinueWith(t => 
            {
                return new LoadDataTaskResultDict<T> { Dict = t.Result.List.GroupBy(x => "").ToDictionary(x => x.Key, x => x.ToList()), HasMore = t.Result.HasMore };
            }), Clear, callback);
        }

        public async void LoadData(Task<LoadDataTaskResultDict<T>> task, bool Clear = true, Action<bool> callback = null)
        {
            try
            {
                var result = await task;

                if (Clear)
                    ClearList();

                foreach (var kvp in result.Dict)
                {
                    var section = kvp.Key;
                    List<T> list;

                    if (!TableList.TryGetValue(section, out list))
                    {
                        list = new List<T>();
                        Sections.Insert(Sections.Count - 1, section); // empty section must be last, insert before last
                        TableList.Add(section, list);
                    };

                    list.AddRange(kvp.Value);
                    Count += kvp.Value.Count;
                };

                HasMore = result.HasMore;
                ReloadData();

                if (callback != null)
                    callback(true);
            } catch (Exception ex)
            {
                if (HasMore)
                {
                    HasMore = false;
                    ReloadData();
                };

                MyLogger.Log(ex);
                if (callback != null)
                    callback(false);
            } finally
            {
                EndRefresh();
            };
        }

        public class ListDataSource : UITableViewDataSource
        {
			DFListTableView<T, TCell> Parent;

			public ListDataSource(DFListTableView<T, TCell> parent)
            {
                Parent = parent;
            }

            public override int NumberOfSections(UITableView tableView)
            {
                return Parent.Sections.Count;
            }

            public override int RowsInSection(UITableView tableView, int section)
            {
                var sectionKey = Parent.Sections[section];
                return Math.Max(sectionKey == "" && Parent.Count == 0 ? 1 : 0,  // no result row
                    Parent.TableList[sectionKey].Count + // object list rows
                    (sectionKey == "" && Parent.HasMore ? 1 : 0)); // loading... row
            }

            public override string TitleForHeader(UITableView tableView, int section)
            {
                return Parent.Sections[section];
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                if (indexPath.Row < Parent.TableList[Parent.Sections[indexPath.Section]].Count)
                {
                    var obj = Parent.TableList[Parent.Sections[indexPath.Section]][indexPath.Row];

					var cell = tableView.DequeueReusableCell ("listCell") as TCell;
                    if (cell == null)
						cell = new TCell();

					cell.SetObject (obj);

                    return cell;
                }
                else
                {
                    var cell = tableView.DequeueReusableCell("moreCell");
                    if (cell == null)
                        cell = new UITableViewCell(UITableViewCellStyle.Default, "moreCell");

                    cell.TextLabel.Font = UIFont.SystemFontOfSize(12);
                    cell.TextLabel.TextAlignment = UITextAlignment.Center;
                    cell.TextLabel.Text = Parent.HasMore ? "Loading...".tr() : "No result".tr();
                    cell.SelectionStyle = UITableViewCellSelectionStyle.None;

                    return cell;
                }
            }
        }

        public class ListDelegate : UITableViewDelegate
        {
			DFListTableView<T, TCell> Parent;

			public ListDelegate(DFListTableView<T, TCell> parent)
            {
                Parent = parent;
            }

            public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath)
            {
                var sectionKey = Parent.Sections[indexPath.Section];

                if (sectionKey == "" && indexPath.Row >= Parent.TableList[sectionKey].Count && Parent.HasMore)
                {
                    Parent.DoLoadMore();
                }
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                Parent.DoRowClicked(indexPath.Section, indexPath.Row);
                tableView.DeselectRow(indexPath, false);
            }
        }
    }

	public class DFListTableViewCell<T> : UITableViewCell
	{
		public const string id = "listCell";

		public DFListTableViewCell() : this(UITableViewCellStyle.Default)
		{

		}

		public DFListTableViewCell(UITableViewCellStyle style) : base (style, id)
		{
		}

		public virtual void SetObject(T obj)
		{
		}
	}

    public class LoadDataTaskResultDict<T>
    {
        // section -> object list
        public Dictionary<string, List<T>> Dict = null;
        public bool HasMore = false;
    }

    public class LoadDataTaskResultList<T>
    {
        // object list
        public List<T> List = null;
        public bool HasMore = false;
    }

    public class RowClickedEventArgs<T> : EventArgs
    {
        public T Object;
    }

    public class SearchClickedEventArgs : EventArgs
    {
        public string Text;
    }
}

