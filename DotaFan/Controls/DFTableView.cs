﻿using System;
using MonoTouch.UIKit;

namespace DotaFan
{
    public class DFTableView : UITableView
    {
        public DFTableView()
        {
            Initialize();
        }

        public DFTableView(System.Drawing.RectangleF frame) : base(frame)
        {
            Initialize();
        }
        

        public DFTableView(System.Drawing.RectangleF frame, UITableViewStyle style) : base(frame, style)
        {
            Initialize();
        }

        private void Initialize()
        {
            if (RespondsToSelector(new MonoTouch.ObjCRuntime.Selector("setSeparatorInset:")))
                SeparatorInset = new UIEdgeInsets(0, 15, 0, 15);

            TableFooterView = new UIView(); // fix for empty cells
        }
    }
}

