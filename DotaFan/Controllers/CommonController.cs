﻿using System;
using MonoTouch.UIKit;

namespace DotaFan
{
    public class CommonController : UIViewController
    {
        protected bool bCreated = false;

        public CommonController()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            if (!bCreated)
                Create();

            View.BackgroundColor = UIColor.White;
        }

        protected virtual void Create()
        {
            bCreated = true;
        }
    }
}

