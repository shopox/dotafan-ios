﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using DotaFan.Model;
using DotaFan.Model.JSON;

namespace DotaFan
{
    public class TournamentsController : CommonController
    {
        string Filter = "";
        DFListTableView<Tournament, TournamentCell> tableView;

        public TournamentsController()
        {
            Title = "Tournaments".tr();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.AddSubview(tableView);
        }

        protected override void Create()
        {
            base.Create();

            tableView = new DFListTableView<Tournament, TournamentCell>(new RectangleF(0, 0, View.Frame.Width, View.Frame.Height));
            tableView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            tableView.HasSearchBar = true;
            tableView.RowHeight = 36;
            tableView.LoadMore += (sender, e) =>
            {
                LoadData(true);
            };
            tableView.RowClicked += (sender, e) => 
            {
                var vc = new TournamentController(e.Object);
                NavigationController.PushViewController(vc, true);
            };
            tableView.Refresh += (sender, e) => 
            {
                LoadData();
            };
            tableView.SearchClicked += (sender, e) => 
            {
                Filter = e.Text;
                View.EndEditing(true);
                LoadData();
            };
        }

        private void LoadData(bool LoadMore = false)
        {
            var repository = new ApiRepository();
            var page = LoadMore ? tableView.Count / ApiRepository.PerPage + 1 : 1;

            tableView.LoadData(repository.GetTournaments(page, Filter).ContinueWith(t =>
            {
                return new LoadDataTaskResultList<Tournament> { List = t.Result.Tournaments, HasMore = t.Result.Tournaments.Count >= ApiRepository.PerPage };
            }), page <= 1);
        }

        public class TournamentCell : DFListTableViewCell<Tournament>
        {
            public TournamentCell()
            {
                SelectionStyle = UITableViewCellSelectionStyle.Gray;
                TextLabel.Font = UIFont.SystemFontOfSize(12);
            }

            public override void SetObject(Tournament obj)
            {
                TextLabel.Text = string.IsNullOrEmpty(obj.Name) ? string.Format("ID {0}", obj.Id) : obj.Name;
            }
        }
    }
}

