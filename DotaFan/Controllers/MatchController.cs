﻿using System;
using System.Drawing;
using MonoTouch.UIKit;
using DotaFan.Model;
using DotaFan.Model.JSON;

namespace DotaFan
{
    public class MatchController : CommonController
    {
        public Match Match { get; private set; }

        UILabel dateLabel, resultLabel, formatLabel;
        UIView separator2;
        TeamView team1View, team2View;
        TournamentBracketView tournamentBracketView;
        UIScrollView scrollView;
        UIRefreshControl rc;

        public MatchController(Match match)
        {
            Title = "Match".tr();
            Match = match;
        }

        private async void Load()
        {
            if (Match == null)
                return;

            var repository = new ApiRepository();

            try
            {
                var response = await repository.GetMatch(Match.Id);
                Match = response.Match;
                Update();
            } catch (Exception ex)
            {
                MyLogger.Log(ex);
            } finally
            {
                rc.EndRefreshing();
            };
        }

        private void Update()
        {
            if (Match == null)
                return;

            float y = 12;
            tournamentBracketView.SetTop(y);
            tournamentBracketView.SetInfo(Match.Tournament, Match.Bracket);
            y += tournamentBracketView.Frame.Height;

            team1View.SetTop(y);
            team2View.SetTop(y);
            resultLabel.SetTop(y + 39);

            team1View.SetTeam(Match.Team1);
            team2View.SetTeam(Match.Team2);

            resultLabel.Text = Match.Result ?? "";

            y += Math.Max(team1View.Frame.Height, team2View.Frame.Height) + 8;
            separator2.SetTop(y);
            y += 14;

            if (Match.Date.Ticks > 0)
            {
                dateLabel.SetTop(y);
                dateLabel.Text = Match.Date.ToString("d.MM.yyyy HH:mm");
                y += 18 + 7;
            }
            else
            {
                dateLabel.Text = "";
            };

            formatLabel.SetTop(y);
            formatLabel.Text = Match.Format ?? "";

            y += formatLabel.Frame.Height;
            scrollView.ContentSize = new SizeF(View.Frame.Width, y);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Load();
            View.AddSubview(scrollView);
        }

        protected override void Create()
        {
            tournamentBracketView = new TournamentBracketView();
            tournamentBracketView.Frame = new RectangleF(13, 0, 294, 0);
            tournamentBracketView.Clicked += (sender, e) => 
            {
                if (Match != null && Match.Tournament != null)
                {
                    var vc = new TournamentController(Match.Tournament);
                    NavigationController.PushViewController(vc, true);
                };
            };

            separator2 = new UIView();
            separator2.BackgroundColor = UIColor.FromRGB(0xe9, 0xe9, 0xeb);
            separator2.Frame = new RectangleF(14, 0, 291, 1);

            team1View = new TeamView();
            team1View.Frame = new RectangleF(13, 0, 125, 100);
            team1View.Clicked += (sender, e) => 
            {
                if (Match != null)
                {
                    var vc = new TeamController(Match.Team1);
                    NavigationController.PushViewController(vc, true);
                };
            };

            team2View = new TeamView();
            team2View.Frame = new RectangleF(183, 0, 125, 100);
            team2View.Clicked += (sender, e) => 
            {
                if (Match != null)
                {
                    var vc = new TeamController(Match.Team2);
                    NavigationController.PushViewController(vc, true);
                };
            };

            resultLabel = new UILabel();
            resultLabel.BackgroundColor = UIColor.Clear;
            resultLabel.Frame = new RectangleF(138, 0, 44, 23);
            resultLabel.TextAlignment = UITextAlignment.Center;
            resultLabel.Font = UIFont.BoldSystemFontOfSize(12);
            resultLabel.TextColor = UIColor.Black;

            dateLabel = new UILabel();
            dateLabel.BackgroundColor = UIColor.Clear;
            dateLabel.Frame = new RectangleF(13, 0, 294, 18);
            dateLabel.TextAlignment = UITextAlignment.Center;
            dateLabel.Font = UIFont.SystemFontOfSize(15);
            dateLabel.TextColor = UIColor.FromRGB(0x8c, 0x8d, 0x8e);

            formatLabel  = new UILabel();
            formatLabel.BackgroundColor = UIColor.Clear;
            formatLabel.Frame = new RectangleF(13, 0, 294, 18);
            formatLabel.TextAlignment = UITextAlignment.Center;
            formatLabel.Font = UIFont.SystemFontOfSize(15);
            formatLabel.TextColor = UIColor.Black;

            scrollView = new UIScrollView();
            scrollView.Frame = new RectangleF(0, 0, View.Frame.Width, View.Frame.Height);
            scrollView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            scrollView.AlwaysBounceVertical = true; // for refreshcontrol

            rc = new UIRefreshControl();
            rc.ValueChanged += (sender, e) =>
            {
                Load();
            };
            scrollView.AddSubview(rc);

            scrollView.AddSubview(tournamentBracketView);
            scrollView.AddSubview(separator2);
            scrollView.AddSubview(dateLabel);
            scrollView.AddSubview(team1View);
            scrollView.AddSubview(team2View);
            scrollView.AddSubview(resultLabel);
            scrollView.AddSubview(formatLabel);

            Update();
        }

        public class TeamView : UIView
        {
            private UIImageView logoIV;
            private UILabel teamLabel;
            public event EventHandler Clicked;

            public TeamView()
            {
                Frame = new RectangleF(0, 0, 125, 100);
                UserInteractionEnabled = true;

                logoIV = new UIImageView();
                logoIV.Frame = new RectangleF(0, 0, 125, 75);
                AddSubview(logoIV);

                teamLabel = new UILabel();
                teamLabel.BackgroundColor = UIColor.Clear;
                teamLabel.Frame = new RectangleF(0, 80, 125, 24);
                teamLabel.TextAlignment = UITextAlignment.Center;
                teamLabel.Font = UIFont.SystemFontOfSize(15);
                teamLabel.TextColor = UIColor.Black;
                teamLabel.HighlightedTextColor = teamLabel.TextColor.ColorWithAlpha(0.5f);
                teamLabel.Lines = 0;
                AddSubview(teamLabel);

                var tapGestureRecognizer = new UITapGestureRecognizer(() =>
                {
                    if (Clicked != null)
                        Clicked(this, EventArgs.Empty);
                });
                AddGestureRecognizer(tapGestureRecognizer);
            }

            public bool Highlighted
            {
                get
                {
                    return teamLabel.Highlighted;
                }
                set
                {
                    teamLabel.Highlighted = value;
                }
            }

            public override void TouchesBegan(MonoTouch.Foundation.NSSet touches, UIEvent evt)
            {
                base.TouchesBegan(touches, evt);
                Highlighted = true;
            }

            public override void TouchesCancelled(MonoTouch.Foundation.NSSet touches, UIEvent evt)
            {
                base.TouchesCancelled(touches, evt);
                Highlighted = false;
            }

            public override void TouchesEnded(MonoTouch.Foundation.NSSet touches, UIEvent evt)
            {
                base.TouchesEnded(touches, evt);
                Highlighted = false;
            }

            public void SetTeam(Team team)
            {
                LoadTeamLogo(team);
                //logoIV.Image = team.GetLogoImage();
                teamLabel.Text = team.Name ?? "";
                teamLabel.SetHeight(teamLabel.SizeThatFits(new SizeF(125, 200)).Height);
                this.SetHeight(teamLabel.Frame.Bottom);
            }

            private async void LoadTeamLogo(Team team)
            {
                logoIV.Image = await team.GetLogoImage();
            }
        }

        public class TournamentBracketView : UIView
        {
            public event EventHandler Clicked;
            private UILabel tournamentNameLabel, bracketLabel;
            private UIView separator;

            public TournamentBracketView()
            {
                tournamentNameLabel = new UILabel();
                tournamentNameLabel.BackgroundColor = UIColor.Clear;
                tournamentNameLabel.Frame = new RectangleF(13, 0, 294, 18);
                tournamentNameLabel.TextAlignment = UITextAlignment.Center;
                tournamentNameLabel.Font = UIFont.SystemFontOfSize(15);
                tournamentNameLabel.TextColor = UIColor.FromRGB(0xb3, 0x1d, 0x1d);
                tournamentNameLabel.HighlightedTextColor = tournamentNameLabel.TextColor.ColorWithAlpha(0.5f);
                AddSubview(tournamentNameLabel);

                bracketLabel  = new UILabel();
                bracketLabel.BackgroundColor = UIColor.Clear;
                bracketLabel.Frame = new RectangleF(13, 0, 294, 18);
                bracketLabel.TextAlignment = UITextAlignment.Center;
                bracketLabel.Font = UIFont.SystemFontOfSize(15);
                bracketLabel.TextColor = UIColor.FromRGB(0x8c, 0x8d, 0x8e);
                bracketLabel.HighlightedTextColor = bracketLabel.TextColor.ColorWithAlpha(0.5f);
                AddSubview(bracketLabel);

                separator = new UIView();
                separator.BackgroundColor = UIColor.FromRGB(0xe9, 0xe9, 0xeb);
                separator.Frame = new RectangleF(0, 0, 291, 1);
                AddSubview(separator);

                var tapGestureRecognizer = new UITapGestureRecognizer(() =>
                {
                    if (Clicked != null)
                        Clicked(this, EventArgs.Empty);
                });
                AddGestureRecognizer(tapGestureRecognizer);
            }

            public bool Highlighted
            {
                get
                {
                    return tournamentNameLabel.Highlighted;
                }
                set
                {
                    tournamentNameLabel.Highlighted = value;
                    bracketLabel.Highlighted = value;
                }
            }

            public override void TouchesBegan(MonoTouch.Foundation.NSSet touches, UIEvent evt)
            {
                base.TouchesBegan(touches, evt);
                Highlighted = true;
            }

            public override void TouchesCancelled(MonoTouch.Foundation.NSSet touches, UIEvent evt)
            {
                base.TouchesCancelled(touches, evt);
                Highlighted = false;
            }

            public override void TouchesEnded(MonoTouch.Foundation.NSSet touches, UIEvent evt)
            {
                base.TouchesEnded(touches, evt);
                Highlighted = false;
            }

            public void SetInfo(Tournament tournament, string bracket)
            {
                float y = 0;

                // tournament
                if (tournament != null && !string.IsNullOrEmpty(tournament.Name))
                {
                    tournamentNameLabel.SetTop(y);
                    tournamentNameLabel.Text = tournament.Name;
                    y += 18 + 7;
                }
                else
                {
                    tournamentNameLabel.Text = "";
                };

                // bracket
                if (!string.IsNullOrEmpty(bracket))
                {
                    bracketLabel.SetTop(y);
                    bracketLabel.Text = bracket;
                    y += 18 + 11;
                }
                else
                {
                    bracketLabel.Text = "";
                };

                // has tournament or bracket -> separator1
                if (!string.IsNullOrEmpty(tournamentNameLabel.Text) || !string.IsNullOrEmpty(bracketLabel.Text))
                {
                    separator.SetTop(y);
                    separator.Hidden = false;
                    y += 14;
                }
                else
                {
                    separator.Hidden = true;
                };

                this.SetHeight(y);
            }
        }
    }
}

