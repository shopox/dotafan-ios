﻿using System;
using MonoTouch.UIKit;

namespace DotaFan
{
    public class MeController : CommonController
    {
        public MeController()
        {
            Title = "Me".tr();
        }
    }
}

