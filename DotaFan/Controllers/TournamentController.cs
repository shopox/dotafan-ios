﻿using System;
using System.Linq;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using DotaFan.Model;
using DotaFan.Model.JSON;

namespace DotaFan
{
    public class TournamentController : CommonController
    {
        public Tournament Tournament { get; private set; }

        DFListTableView<Match, MatchesController.MatchCell> tableView;

        public TournamentController(Tournament tournament)
        {
            Title = "Tournament".tr();
            Tournament = tournament;
        }

        private void LoadData(bool LoadMore = false)
        {
            if (Tournament == null)
                return;

            var repository = new ApiRepository();
            var page = LoadMore ? tableView.Count / ApiRepository.PerPage + 1 : 1;

            tableView.LoadData(repository.GetMatches(page, tournamentId: Tournament.Id).ContinueWith(t =>
            {
                return new LoadDataTaskResultDict<Match> { Dict = t.Result.Matches.GroupBy(m => m.MatchDateString()).ToDictionary(x => x.Key, x => x.ToList()), HasMore = t.Result.Matches.Count >= ApiRepository.PerPage };
            }), page <= 1);
        }

        protected override void Create()
        {
            base.Create();

            tableView = new DFListTableView<Match, MatchesController.MatchCell>(new RectangleF(0, 0, View.Frame.Width, View.Frame.Height));
            tableView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            tableView.RowHeight = 36;
            tableView.LoadMore += (sender, e) =>
            {
                LoadData(true);
            };
            tableView.RowClicked += (sender, e) => 
            {
                var vc = new MatchController(e.Object);
                NavigationController.PushViewController(vc, true);
            };
            tableView.Refresh += (sender, e) => 
            {
                LoadData();
            };

            var headerView = new UIView(new RectangleF(0, 0, View.Frame.Width, 40));
            var nameLabel  = new UILabel();
            nameLabel.BackgroundColor = UIColor.Clear;
            nameLabel.Frame = new RectangleF(10, 0, headerView.Frame.Width - 20, headerView.Frame.Height);
            nameLabel.TextAlignment = UITextAlignment.Center;
            nameLabel.Font = UIFont.SystemFontOfSize(15);
            nameLabel.TextColor = UIColor.Black;
            nameLabel.Text = Tournament.Name;
            headerView.AddSubview(nameLabel);

            NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromSeconds(10), () =>
            {
                RefreshTableStatus();
            });

            tableView.TableHeaderView = headerView;
        }

        private void RefreshTableStatus()
        {
            foreach (var cell in tableView.VisibleCells)
            {
                var mcell = cell as MatchesController.MatchCell;
                if (mcell != null)
                {
                    mcell.RefreshStatus();
                };
            };
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.AddSubview(tableView);
        }
    }
}

