﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using DotaFan.Model;
using DotaFan.Model.JSON;

namespace DotaFan
{
    public class TeamController : CommonController
    {
        public Team Team { get; private set; }

        DFListTableView<Match, MatchesController.MatchCell> tableView;
        TeamInfoView teamInfoView;

        public TeamController(Team team)
        {
            Title = "Team".tr();
            Team = team;
        }

        private void LoadData(bool LoadMore = false)
        {
            if (Team == null)
                return;

            LoadTeamData();

            var repository = new ApiRepository();
            var page = LoadMore ? tableView.Count / ApiRepository.PerPage + 1 : 1;

            tableView.LoadData(repository.GetMatches(page, teamId: Team.Id).ContinueWith(t =>
            {
                return new LoadDataTaskResultDict<Match> { Dict = t.Result.Matches.GroupBy(m => m.MatchDateString()).ToDictionary(x => x.Key, x => x.ToList()), HasMore = t.Result.Matches.Count >= ApiRepository.PerPage };
            }), page <= 1);
        }

        private async void LoadTeamData()
        {
            if (Team == null)
                return;

            var repository = new ApiRepository();

            try
            {
                var response = await repository.GetTeam(Team.Id);
                Team = response.Team;
                Update();
            } catch (Exception ex)
            {
                MyLogger.Log(ex);
            };
        }

        private void Update()
        {
            if (Team == null)
                return;

            Title = "Team".tr();
            teamInfoView.SetTeam(Team);
            tableView.TableHeaderView = teamInfoView;
        }

        protected override void Create()
        {
            base.Create();

            tableView = new DFListTableView<Match, MatchesController.MatchCell>(new RectangleF(0, 0, View.Frame.Width, View.Frame.Height));
            tableView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            tableView.RowHeight = 36;
            tableView.LoadMore += (sender, e) =>
            {
                LoadData(true);
            };
            tableView.RowClicked += (sender, e) => 
            {
                var vc = new MatchController(e.Object);
                NavigationController.PushViewController(vc, true);
            };
            tableView.Refresh += (sender, e) => 
            {
                LoadData();
            };

            teamInfoView = new TeamInfoView();
            teamInfoView.SetWidth(View.Frame.Width);
            teamInfoView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            tableView.TableHeaderView = teamInfoView;

            NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromSeconds(10), () =>
            {
                RefreshTableStatus();
            });

            Update();
        }

        private void RefreshTableStatus()
        {
            foreach (var cell in tableView.VisibleCells)
            {
                var mcell = cell as MatchesController.MatchCell;
                if (mcell != null)
                {
                    mcell.RefreshStatus();
                };
            };
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.AddSubview(tableView);
        }

        public class PlayerInfoView : UIView
        {
            UILabel nameLabel, nickLabel, roleLabel;
            UIImageView playerFlag;

            public PlayerInfoView()
            {
                Frame = new RectangleF(0, 0, 160, 63);

                playerFlag = new UIImageView();
                playerFlag.Frame = new RectangleF(13, 0, 24, 24);
                AddSubview(playerFlag);

                nickLabel  = new UILabel();
                nickLabel.BackgroundColor = UIColor.Clear;
                nickLabel.Frame = new RectangleF(playerFlag.Frame.Right + 5, 0, Frame.Width - playerFlag.Frame.Right - 15, 24);
                nickLabel.TextAlignment = UITextAlignment.Left;
                nickLabel.Font = UIFont.BoldSystemFontOfSize(14);
                nickLabel.TextColor = UIColor.Black;
                AddSubview(nickLabel);

                nameLabel  = new UILabel();
                nameLabel.BackgroundColor = UIColor.Clear;
                nameLabel.Frame = new RectangleF(13, nickLabel.Frame.Bottom, Frame.Width - 20, 13);
                nameLabel.TextAlignment = UITextAlignment.Left;
                nameLabel.Font = UIFont.BoldSystemFontOfSize(12);
                nameLabel.TextColor = UIColor.FromRGB(0x8c, 0x8d, 0x8e);
                AddSubview(nameLabel);

                roleLabel  = new UILabel();
                roleLabel.BackgroundColor = UIColor.Clear;
                roleLabel.Frame = new RectangleF(13, nameLabel.Frame.Bottom, Frame.Width - 20, 13);
                roleLabel.TextAlignment = UITextAlignment.Left;
                roleLabel.Font = UIFont.SystemFontOfSize(12);
                roleLabel.TextColor = UIColor.FromRGB(0x8c, 0x8d, 0x8e);
                AddSubview(roleLabel);
            }

            public void SetPlayer(Player player)
            {
                nameLabel.Text = player.Name ?? "";
                nickLabel.Text = player.Nickname ?? "";
                roleLabel.Text = player.GetRoleName();
                playerFlag.Image = Flag.GetImageForCountry(player.Country);
            }
        }

        public class TeamInfoView : UIView
        {
            private UIImageView logoIV;
            UILabel nameLabel, rosterLabel, matchesLabel, countryLabel, tagTitleLabel, tagLabel;
            UIImageView teamFlag;
            List<UIView> rosterViews = new List<UIView>();
            UIView nameSeparator, rosterSeparator, matchesSeparator;

            public TeamInfoView()
            {
                Frame = new RectangleF(0, 0, 320, 141);
                nameLabel  = new UILabel();
                nameLabel.BackgroundColor = UIColor.Clear;
                nameLabel.Frame = new RectangleF(13, 0, Frame.Width - 13 * 2, 40);
                nameLabel.TextAlignment = UITextAlignment.Center;
                nameLabel.Font = UIFont.BoldSystemFontOfSize(18);
                nameLabel.TextColor = UIColor.Black;
                AddSubview(nameLabel);

                nameSeparator = new UIView();
                nameSeparator.BackgroundColor = UIColor.FromRGB(0xe9, 0xe9, 0xeb);
                nameSeparator.Frame = new RectangleF(13, 39, 290, 1);
                AddSubview(nameSeparator);

                rosterLabel = new UILabel();
                rosterLabel.BackgroundColor = UIColor.Clear;
                rosterLabel.Frame = new RectangleF(13, 141, Frame.Width - 13 * 2, 20);
                rosterLabel.TextAlignment = UITextAlignment.Left;
                rosterLabel.Font = UIFont.BoldSystemFontOfSize(15);
                rosterLabel.TextColor = UIColor.FromRGB(0x8c, 0x8d, 0x8e);;
                rosterLabel.Text = "Roster".tr();
                AddSubview(rosterLabel);

                rosterSeparator = new UIView();
                rosterSeparator.BackgroundColor = UIColor.FromRGB(0xe9, 0xe9, 0xeb);
                rosterSeparator.Frame = new RectangleF(13, rosterLabel.Frame.Bottom, 290, 1);
                AddSubview(rosterSeparator);

                matchesLabel = new UILabel();
                matchesLabel.BackgroundColor = UIColor.Clear;
                matchesLabel.Frame = new RectangleF(13, rosterLabel.Frame.Top, Frame.Width - 13 * 2, 20);
                matchesLabel.TextAlignment = UITextAlignment.Left;
                matchesLabel.Font = UIFont.SystemFontOfSize(15);
                matchesLabel.TextColor = UIColor.FromRGB(0x8c, 0x8d, 0x8e);
                matchesLabel.Text = "Matches".tr();
                AddSubview(matchesLabel);

                matchesSeparator = new UIView();
                matchesSeparator.BackgroundColor = UIColor.FromRGB(0xe9, 0xe9, 0xeb);
                matchesSeparator.Frame = new RectangleF(13, matchesLabel.Frame.Bottom, 290, 1);
                AddSubview(matchesSeparator);

                teamFlag = new UIImageView();
                teamFlag.Frame = new RectangleF(152, 48, 24, 24);
                AddSubview(teamFlag);

                countryLabel  = new UILabel();
                countryLabel.BackgroundColor = UIColor.Clear;
                countryLabel.Frame = new RectangleF(teamFlag.Frame.Right + 5, teamFlag.Frame.Top, Frame.Width - teamFlag.Frame.Right - 15, 24);
                countryLabel.TextAlignment = UITextAlignment.Left;
                countryLabel.Font = UIFont.SystemFontOfSize(15);
                countryLabel.TextColor = UIColor.Black;
                AddSubview(countryLabel);

                tagTitleLabel = new UILabel();
                tagTitleLabel.BackgroundColor = UIColor.Clear;
                tagTitleLabel.Frame = new RectangleF(152, 77, 0, 0);
                tagTitleLabel.TextAlignment = UITextAlignment.Left;
                tagTitleLabel.Font = UIFont.SystemFontOfSize(15);
                tagTitleLabel.TextColor = UIColor.FromRGB(0x8c, 0x8d, 0x8e);
                tagTitleLabel.Text = "Tag:".tr();
                tagTitleLabel.SizeToFit();
                AddSubview(tagTitleLabel);

                tagLabel  = new UILabel();
                tagLabel.BackgroundColor = UIColor.Clear;
                tagLabel.Frame = new RectangleF(tagTitleLabel.Frame.Right + 5, tagTitleLabel.Frame.Top, Frame.Width - tagTitleLabel.Frame.Right - 15, tagTitleLabel.Frame.Height);
                tagLabel.TextAlignment = UITextAlignment.Left;
                tagLabel.Font = UIFont.SystemFontOfSize(15);
                tagLabel.TextColor = UIColor.Black;
                AddSubview(tagLabel);

                logoIV = new UIImageView();
                logoIV.Frame = new RectangleF(13, 53, 125, 75);
                AddSubview(logoIV);
            }

            private void ClearRoster()
            {
                foreach (var playerInfoView in rosterViews)
                    playerInfoView.RemoveFromSuperview();

                rosterViews.Clear();
            }

            private async void LoadTeamLogo(Team team)
            {
                logoIV.Image = await team.GetLogoImage();
            }

            public void SetTeam(Team team)
            {
                teamFlag.Image = Flag.GetImageForCountry(team.Country);
                //logoIV.Image = team.GetLogoImage();
                LoadTeamLogo(team);
                nameLabel.Text = team.Name;
                countryLabel.Text = team.GetCountryName();
                tagLabel.Text = team.ShortName;

                ClearRoster();
                float y = rosterLabel.Frame.Top;
                if (team.Roster != null && team.Roster.Count > 0)
                {
                    rosterLabel.Hidden = rosterSeparator.Hidden = false;
                    y += rosterLabel.Frame.Height + 6;
                    var x = 0f;
                    int i = 0;
                    foreach (var player in team.Roster)
                    {
                        var playerInfoView = new PlayerInfoView();
                        playerInfoView.SetPlayer(player);
                        playerInfoView.Frame = new RectangleF(x, y, Frame.Width / 2, playerInfoView.Frame.Height);
                        playerInfoView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
                        AddSubview(playerInfoView);

                        rosterViews.Add(playerInfoView);
                        x += Frame.Width / 2;

                        bool isLast = i == team.Roster.Count - 1;
                        if (x >= Frame.Width || isLast)
                        {
                            x = 0;
                            y += playerInfoView.Frame.Height;

                            if (!isLast)
                            {
                                var separator = new UIView();
                                separator.BackgroundColor = UIColor.FromRGB(0xe9, 0xe9, 0xeb);
                                separator.Frame = new RectangleF(13, y - 5, 290, 1);
                                AddSubview(separator);
                                rosterViews.Add(separator);
                            }
                        };

                        i++;
                    }
                }
                else
                    rosterLabel.Hidden = rosterSeparator.Hidden = true;

                matchesLabel.SetTop(y);
                matchesSeparator.SetTop(matchesLabel.Frame.Bottom);
                y += matchesLabel.Frame.Height + 6;

                this.SetHeight(y);
            }
        }
    }
}
