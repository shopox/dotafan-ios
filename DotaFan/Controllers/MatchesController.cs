﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using DotaFan.Model;
using DotaFan.Model.JSON;

namespace DotaFan
{
    public class MatchesController : CommonController
    {
        UISegmentedControl segmentedControl;
		List<DFListTableView<Match, MatchCell>> tableViews;
        UIToolbar switchToolbar;

        public MatchesController()
        {
            Title = "Matches".tr();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.AddSubview(switchToolbar);
            foreach (var tableView in tableViews)
                View.AddSubview(tableView);
        }

        private class SwitchTBDelegate : UIToolbarDelegate
        {
            public override UIBarPosition GetPositionForBar(IUIBarPositioning barPositioning)
            {
                return UIBarPosition.TopAttached;
            }
        }

        private void ShowHideNavBarHairLine(bool visible)
        {
            FindShowHideIV(NavigationController.NavigationBar, visible);
        }

        private void FindShowHideIV(UIView view, bool visible, int level = 0)
        {
            foreach (var v in view.Subviews)
            {
                if (v is UIImageView && v.Frame.Height < 2)
                {
                    v.Hidden = !visible;
                };
                FindShowHideIV(v, visible, level + 1);
            };
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            ShowHideNavBarHairLine(false);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            ShowHideNavBarHairLine(true);
        }

        protected override void Create()
        {
            base.Create();

            segmentedControl = new UISegmentedControl();
            segmentedControl.InsertSegment("Upcoming".tr(), 0, false);
            segmentedControl.InsertSegment("Live".tr(), 1, false);
            segmentedControl.InsertSegment("Past".tr(), 2, false);

            switchToolbar = new UIToolbar();
            switchToolbar.Frame = new RectangleF(0, 0, View.Frame.Width, 40);
            switchToolbar.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;

            if (switchToolbar.RespondsToSelector(new MonoTouch.ObjCRuntime.Selector("setBarTintColor:")))
                switchToolbar.BarTintColor = UIColor.FromRGB(0xb3, 0x1d, 0x1d);

            switchToolbar.Delegate = new SwitchTBDelegate();
            switchToolbar.Translucent = false;
            switchToolbar.AddSubview(segmentedControl);
            segmentedControl.TintColor = UIColor.White;

            segmentedControl.Frame = new RectangleF(10, 5, View.Frame.Width - 20, 30);
            segmentedControl.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            segmentedControl.ValueChanged += (sender, e) => 
            {
                SetTab((MatchFilter)segmentedControl.SelectedSegment);
            };

			tableViews = new List<DFListTableView<Match, MatchCell>>();
            for (int i = 0; i < segmentedControl.NumberOfSegments; i++)
            {
                var tableView = new DFListTableView<Match, MatchCell>(new RectangleF(0, 40, View.Frame.Width, View.Frame.Height - 40));
                tableView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
                tableView.RowHeight = 36;
                tableView.Tag = i;
                tableView.LoadMore += (sender, e) =>
                {
                    LoadData((MatchFilter)tableView.Tag, true);
                };
                tableView.RowClicked += (sender, e) => 
                {
                    var vc = new MatchController(e.Object);
                    NavigationController.PushViewController(vc, true);
                };
                tableView.Refresh += (sender, e) => 
                {
                    LoadData((MatchFilter)tableView.Tag);
                };

                tableViews.Add(tableView);
            }

            SetTab(MatchFilter.Live);
            NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromSeconds(10), () =>
            {
                RefreshTableStatus((MatchFilter)segmentedControl.SelectedSegment);
            });
            NSTimer.CreateRepeatingScheduledTimer(TimeSpan.FromMinutes(1), () =>
            {
                LoadData(MatchFilter.Upcoming); // upcoming
                LoadData(MatchFilter.Live); // live
            });
        }

        private DFListTableView<Match, MatchCell> GetTableView(MatchFilter filter)
        {
            return tableViews[(int)filter];
        }

        private void RefreshTableStatus(MatchFilter filter)
        {
            var tableView = GetTableView(filter);

            foreach (var cell in tableView.VisibleCells)
            {
                var mcell = cell as MatchCell;
                if (mcell != null)
                {
                    mcell.RefreshStatus();
                };
            };
        }

        private void SetTab(MatchFilter filter)
        {
            segmentedControl.SelectedSegment = (int)filter;
            tableViews.ForEach(tv => tv.Hidden = tv.Tag != (int)filter);
            View.EndEditing(true);
        }

        private void LoadData(MatchFilter filter, bool LoadMore = false)
        {
			var repository = new ApiRepository();

            var tableView = GetTableView(filter);

            switch (filter)
            {
                case MatchFilter.Upcoming:

                    tableView.LoadData(repository.GetUpcomingMatches().ContinueWith(t =>
                    {
                        return t.Result.Matches.GroupBy(m => m.MatchDateString()).ToDictionary(x => x.Key, x => x.ToList()); 
                    }));
                    break;

                case MatchFilter.Live:
                    tableView.LoadData(repository.GetLiveMatches().ContinueWith(t =>
                    {
                        return t.Result.Matches;
                    }));
                    break;

                case MatchFilter.Past:
                    var page = LoadMore ? tableView.Count / ApiRepository.PerPage + 1 : 1;

                    tableView.LoadData(repository.GetPastMatches(page).ContinueWith(t =>
                    {
                        return new LoadDataTaskResultDict<Match> { Dict = t.Result.Matches.GroupBy(m => m.MatchDateString()).ToDictionary(x => x.Key, x => x.ToList()), HasMore = t.Result.Matches.Count >= ApiRepository.PerPage };
                    }), page <= 1);
                    break;
            };
        }

        public enum MatchFilter
        {
            Upcoming = 0,
            Live = 1,
            Past = 2,
        }

		public class MatchCell : DFListTableViewCell<Match>
		{
            UIImageView team1Flag, team2Flag;
            UILabel team1Label, team2Label, vsLabel, statusLabel;
            Match Match;

			public MatchCell()
			{
				SelectionStyle = UITableViewCellSelectionStyle.Gray;

                var h = ContentView.Frame.Height;

                team1Flag = new UIImageView();
                team1Flag.Frame = new RectangleF(15, (h - 24) / 2, 24, 24);
                team1Flag.AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin;
                ContentView.AddSubview(team1Flag);

                team2Flag = new UIImageView();
                team2Flag.Frame = new RectangleF(150, (h - 24) / 2, 24, 24);
                team2Flag.AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin;
                ContentView.AddSubview(team2Flag);

                team1Label = new UILabel();
                team1Label.BackgroundColor = UIColor.Clear;
                team1Label.Font = UIFont.SystemFontOfSize(12);
                team1Label.Frame = new RectangleF(42, 0, 83, h);
                team1Label.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
                ContentView.AddSubview(team1Label);

                vsLabel = new UILabel();
                vsLabel.BackgroundColor = UIColor.Clear;
                vsLabel.TextColor = UIColor.FromRGB(0xa2, 0xa5, 0xa4);
                vsLabel.Font = UIFont.SystemFontOfSize(12);
                vsLabel.Frame = new RectangleF(128, 0, 14, h);
                vsLabel.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
                vsLabel.Text = "vs";
                ContentView.AddSubview(vsLabel);

                team2Label = new UILabel();
                team2Label.BackgroundColor = UIColor.Clear;
                team2Label.Font = UIFont.SystemFontOfSize(12);
                team2Label.Frame = new RectangleF(177, 0, 83, h);
                team2Label.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
                ContentView.AddSubview(team2Label);

                statusLabel = new UILabel();
                statusLabel.BackgroundColor = UIColor.Clear;
                statusLabel.TextColor = UIColor.FromRGB(0xa2, 0xa5, 0xa4);
                statusLabel.Font = UIFont.SystemFontOfSize(12);
                statusLabel.Frame = new RectangleF(262, 0, 58, h);
                statusLabel.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
                ContentView.AddSubview(statusLabel);
			}

			public override void SetObject (Match match)
            {
                Match = match;

                team1Flag.Image = Flag.GetImageForCountry(match.Team1.Country);
                team1Label.Text = match.Team1.ShortName;

                team2Flag.Image = Flag.GetImageForCountry(match.Team2.Country);
                team2Label.Text = match.Team2.ShortName;

                RefreshStatus();
			}

            public void RefreshStatus()
            {
                //int winner = Match.GetWinner();
                //team1Label.Font = winner == 1 ? UIFont.BoldSystemFontOfSize(12) : UIFont.SystemFontOfSize(12);
                //team2Label.Font = winner == 2 ? UIFont.BoldSystemFontOfSize(12) : UIFont.SystemFontOfSize(12);

                statusLabel.Text = Match.GetStatusString();
            }
		}
    }
}

