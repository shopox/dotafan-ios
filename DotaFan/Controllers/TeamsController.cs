﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using DotaFan.Model;
using DotaFan.Model.JSON;

namespace DotaFan
{
    public class TeamsController : CommonController
    {
        string Filter = "";
        DFListTableView<Team, TeamCell> tableView;

        public TeamsController()
        {
            Title = "Teams".tr();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.AddSubview(tableView);
        }

        protected override void Create()
        {
            base.Create();

            tableView = new DFListTableView<Team, TeamCell>(new RectangleF(0, 0, View.Frame.Width, View.Frame.Height));
            tableView.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            tableView.HasSearchBar = true;
            tableView.RowHeight = 36;
            tableView.LoadMore += (sender, e) =>
            {
                LoadData(true);
            };
            tableView.RowClicked += (sender, e) => 
            {
                var vc = new TeamController(e.Object);
                NavigationController.PushViewController(vc, true);
            };
            tableView.Refresh += (sender, e) => 
            {
                LoadData(callback:(r) =>
                {
                    tableView.EndRefresh();
                });
            };
            tableView.SearchClicked += (sender, e) => 
            {
                Filter = e.Text;
                View.EndEditing(true);
                LoadData();
            };
        }

        private void LoadData(bool LoadMore = false, Action<bool> callback = null)
        {
            var repository = new ApiRepository();
            var page = LoadMore ? tableView.Count / ApiRepository.PerPage + 1 : 1;

            tableView.LoadData(repository.GetTeams(page, Filter).ContinueWith(t =>
            {
                return new LoadDataTaskResultList<Team> { List = t.Result.Teams, HasMore = t.Result.Teams.Count >= ApiRepository.PerPage };
            }), page <= 1);
        }

        public class TeamCell : DFListTableViewCell<Team>
        {
            UIImageView teamFlag;
            UILabel teamLabel;

            public TeamCell()
            {
                SelectionStyle = UITableViewCellSelectionStyle.Gray;
                var h = ContentView.Frame.Height;
                var w = ContentView.Frame.Width;

                teamFlag = new UIImageView();
                teamFlag.Frame = new RectangleF(15, (h - 24) / 2, 24, 24);
                teamFlag.AutoresizingMask = UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin;
                ContentView.AddSubview(teamFlag);

                teamLabel = new UILabel();
                teamLabel.BackgroundColor = UIColor.Clear;
                teamLabel.Font = UIFont.SystemFontOfSize(12);
                teamLabel.Frame = new RectangleF(42, 0, w - 42 - 15, h);
                teamLabel.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
                ContentView.AddSubview(teamLabel);
            }

            public override void SetObject(Team team)
            {
                teamFlag.Image = Flag.GetImageForCountry(team.Country);
                teamLabel.Text = team.Name;
            }
        }
    }
}

