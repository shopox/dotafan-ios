﻿using System;
using MonoTouch.UIKit;

namespace DotaFan
{
    public class DFNavController : UINavigationController
    {
        public DFNavController()
        {
            Initialize();
        }

        public DFNavController(UIViewController rootViewController) : base(rootViewController)
        {
            Initialize();
        }

        private void Initialize()
        {
            NavigationBar.BackgroundColor = UIColor.White;
            NavigationBar.Translucent = false;
        }
    }
}

