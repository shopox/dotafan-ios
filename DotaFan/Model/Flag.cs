﻿using System;
using System.IO;
using MonoTouch.UIKit;

namespace DotaFan.Model
{
    public class Flag
    {
        public static string GetFilenameForCountry(string country)
        {
            var unknownFilename = "Flags/_unknown.png";
            if (string.IsNullOrEmpty(country))
                return unknownFilename;

            var filename = string.Format("Flags/{0}.png", country.ToUpper());
            return File.Exists(filename) ? filename : unknownFilename;
        }

        public static UIImage GetImageForCountry(string country)
        {
            return UIImage.FromFile(GetFilenameForCountry(country));
        }

        public static string GetNameForCountry(string country)
        {
            if (string.IsNullOrEmpty(country))
                return "country_none".tr_country();

            return string.Format("country_{0}", country.ToUpper()).tr_country();
        }
    }
}

