﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace DotaFan.Model
{
	public class ApiRepository
	{
		private string _baseUri = "https://easyrares.com/";

        public const int PerPage = 50;

		public ApiRepository ()
		{
		}

		private Type ParseResponse<Type>(string json)
		{
			var errorResponse = JsonConvert.DeserializeObject<JSON.ErrorResponse> (json);
			if (errorResponse.Error != null) {
				throw new Exception (errorResponse.Error.Message);
			}

			var response = JsonConvert.DeserializeObject<Type> (json);

			return response;
		}

		public async Task<JSON.MatchesResponse> GetLiveMatches()
		{
			var jsonString = await HTTP.Get (_baseUri, "api/match/list/live/");

			return ParseResponse<JSON.MatchesResponse> (jsonString);
		}

		public async Task<JSON.MatchesResponse> GetPastMatches(int page = 1)
		{
			var jsonString = await HTTP.Get (_baseUri, "api/match/list/past/" + page.ToString());
			var response = ParseResponse<JSON.MatchesResponse> (jsonString);

			return response;
		}

		public async Task<JSON.MatchesResponse> GetUpcomingMatches()
		{
			var jsonString = await HTTP.Get (_baseUri, "api/match/list/upcoming/");
			var response = ParseResponse<JSON.MatchesResponse> (jsonString);

			return response;
		}

		public async Task<JSON.MatchesResponse> GetMatches(int page = 1, int teamId = 0, int tournamentId = 0)
		{
			var parameters = new Dictionary<string, object>();
			if (teamId > 0) {
				parameters.Add("team_id", teamId);
			}
			if (tournamentId > 0) {
				parameters.Add ("tournament_id", tournamentId);
			}

			var jsonString = await HTTP.Get (_baseUri, "api/match/list/" + page.ToString(), 
				parameters.Values.Count > 0 ? parameters : null);

			var response = ParseResponse<JSON.MatchesResponse> (jsonString);

			return response;
		}

        public async Task<JSON.MatchResponse> GetMatch(int id, string token = "")
		{
            var parameters = new Dictionary<string, object>();
            if (token.Length > 0)
                parameters.Add("token", token);

            var jsonString = await HTTP.Get (_baseUri, "api/match/info/" + id.ToString (),
                parameters.Values.Count > 0 ? parameters : null);

			var response = ParseResponse<JSON.MatchResponse> (jsonString);

			return response;
		}

        public async Task<JSON.TeamResponse> GetTeam(int id, string token = "")
        {
            var parameters = new Dictionary<string, object>();
            if (token.Length > 0)
                parameters.Add("token", token);

            var jsonString = await HTTP.Get (_baseUri, "api/team/info/" + id.ToString(),
                parameters.Values.Count > 0 ? parameters : null);
			
			var response = ParseResponse<JSON.TeamResponse> (jsonString);

			return response;
		}

		public async Task<JSON.TeamsResponse> GetTeams(int page = 1, string name = "")
		{
			var parameters = new Dictionary<string, object>();
			if (name.Length > 0) {
				parameters.Add("name", name);
			}

			var jsonString = await HTTP.Get (_baseUri, "api/team/list/" + page.ToString(), 
				parameters.Values.Count > 0 ? parameters : null);

			//var teamsResponse = JsonConvert.DeserializeObject<JSON.TeamsResponse> (jsonString);
			var response = ParseResponse<JSON.TeamsResponse> (jsonString);

			return response;
		}

		public async Task<JSON.TournamentsResponse> GetTournaments(int page = 1, string name = "")
		{
			var parameters = new Dictionary<string, object>();
			if (name.Length > 0) {
				parameters.Add("name", name);
			}

			var jsonString = await HTTP.Get (_baseUri, "api/tournament/list/" + page.ToString(), 
				parameters.Values.Count > 0 ? parameters : null);

			//var response = JsonConvert.DeserializeObject<JSON.TournamentsResponse> (jsonString);
			var response = ParseResponse<JSON.TournamentsResponse> (jsonString);

			return response;
		}

        public async Task<JSON.TournamentResponse> GetTournament(int id, string token = "")
		{
            var parameters = new Dictionary<string, object>();
            if (token.Length > 0)
                parameters.Add("token", token);

            var jsonString = await HTTP.Get (_baseUri, "api/tournament/info/" + id.ToString(),
                parameters.Values.Count > 0 ? parameters : null);

			var response = ParseResponse<JSON.TournamentResponse> (jsonString);

			return response;
		}

        public async Task<Stream> DownloadTeamLogo(JSON.Team team)
        {
            if (string.IsNullOrEmpty(team.LogoUrl))
                return null;

            return await HTTP.DownloadAsync(_baseUri, team.LogoUrl);
        }
	}
}

