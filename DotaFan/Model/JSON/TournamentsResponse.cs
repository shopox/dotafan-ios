﻿using System;
using System.Collections.Generic;

namespace DotaFan.Model.JSON
{
	public class TournamentsResponse
	{
		public List<Tournament> Tournaments { get; set; }
	}
}

