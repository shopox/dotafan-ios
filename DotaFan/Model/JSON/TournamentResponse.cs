﻿using System;

namespace DotaFan.Model.JSON
{
	public class TournamentResponse
	{
		public Tournament Tournament { get; set; }
		public bool IsFollowing { get; set; }

		public TournamentResponse ()
		{
			IsFollowing = false;
		}
	}
}

