﻿using System;
using System.Collections.Generic;
using MonoTouch.UIKit;
using System.Threading.Tasks;
using MonoTouch.Foundation;

namespace DotaFan.Model.JSON
{
	public class Team
	{
		public int Id { get; set; }
		public string Country { get; set; }
		public string Name { get; set; }
		public string ShortName { get; set; }
		public List<Player> Roster { get; set; }
        public string LogoUrl { get; set; }

        public static UIImage GetNoLogoImage()
        {
            return UIImage.FromFile("Teams/no_logo.png");
        }

        public string GetCountryName()
        {
            return Flag.GetNameForCountry(Country);
        }

        public async Task<UIImage> GetLogoImage()
        {
            if (LogoUrl != null)
            {
                try
                {
                    var repository = new ApiRepository();
                    var stream = await repository.DownloadTeamLogo(this);

                    if (stream != null)
                    {
                        using (stream)
                        {
                            using (var data = NSData.FromStream(stream))
                                return UIImage.LoadFromData(data);
                        }
                    }
                }
                catch 
                {
                }
            }

            return GetNoLogoImage();
        }
	}
}

