﻿using System;
using System.Collections.Generic;

namespace DotaFan.Model.JSON
{
	public class MatchesResponse
	{
		public List<Match> Matches { get; set; }
	}
}

