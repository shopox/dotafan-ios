﻿using System;
using System.Collections.Generic;

namespace DotaFan.Model.JSON
{
	public class Match
	{
		public int Id { get; set; }
		public string Result { get; set; }
		public string Bracket { get; set; }
        public string Format { get; set; }
		public int Finished { get; set; }
		public Tournament Tournament { get; set; }
		public Team Team1 { get; set; }
		public Team Team2 { get; set; }
		public DateTime Date { get; set; }
		public List<Map> Maps { get; set; }

        enum FinishedFlag {
            NotPlayed = 0,
            Result = 1,
            Canceled = 2,
            Postponed = 3,
            TBA = 4
        }

		public class Ban
		{
			public int N { get; set; }
			public string Hero { get; set; }
		}

		public class Pick
		{
			public int N { get; set; }
			public string Hero { get; set; }
			public string Nickname { get; set; }
		}

		public class Map
		{
			public int N { get; set; }
			public string Result { get; set; }

			public List<Ban> Bans1 { get; set; }
			public List<Ban> Bans2 { get; set; }

			public List<Pick> Picks1 { get; set; }
			public List<Pick> Picks2 { get; set; }
		}

        // 0 - no winner, 1 - team 1, 2 - team 2
        public int GetWinner()
        {
            if ((FinishedFlag)Finished == FinishedFlag.Result && !string.IsNullOrEmpty(Result))
            {
                string[] p = Result.Split(':');

                if (p.Length == 2)
                {
                    int r1, r2;
                    if (int.TryParse(p[0], out r1) && int.TryParse(p[1], out r2))
                    {
                        if (r1 == r2)
                            return 0;

                        return r2 > r1 ? 2 : 1;
                    };
                };
            };

            return 0;
        }

        public string GetStatusString()
        {
            switch (Finished) {
                case (int)FinishedFlag.NotPlayed:
                    if (DateTime.Now >= Date)
                        return "mt_live".tr();

                    if (Date.ToString("d") != DateTime.Now.ToString("d"))
                        return Date.ToString("HH:mm");

                    return GetRemaining();

                case (int)FinishedFlag.Result:
                    return Result;

                case (int)FinishedFlag.Canceled:
                    return "mt_cancel".tr();

                case (int)FinishedFlag.Postponed:
                    return "mt_postp".tr();

                case (int)FinishedFlag.TBA:
                    return "mt_tba".tr();

                default:
                    return "";
            }
        }

        public string GetRemaining()
        {
            var ret = "";

            var span = Date - DateTime.Now;
            var minutes = Convert.ToInt32(span.TotalMinutes);
            var days = minutes / (60 * 24);

            if (days > 0)
            {
                ret = days.ToString() + "mt_d".tr();
                minutes -= days * 60 * 24;
            }

            var hours = minutes / 60;
            if (hours > 0)
            {
                if (ret.Length > 0)
                    ret += " ";

                ret += hours.ToString() + "mt_h".tr();
                minutes -= hours * 60;
            }

            if (days > 0)
                return ret;

            if (minutes > 0)
            {
                if (ret.Length > 0)
                    ret += " ";

                ret += minutes.ToString() + "mt_m".tr();
            }
            else if (ret.Length == 0)
            {
                ret = "mt_now".tr();
            }

            return ret;
        }

        public string MatchDateString()
        {
            var date = Date.Date;
            var days = (int)date.Subtract(DateTime.Today).TotalDays;

            switch (days)
            {
                case 0:
                    return "Today".tr();

                case 1:
                    return "Tomorrow".tr();

                case -1:
                    return "Yesterday".tr();

                default:
                    return date.ToString("dd.MM.yyyy");
            };
        }
	}
}

