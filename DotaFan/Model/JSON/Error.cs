﻿using System;

namespace DotaFan.Model.JSON
{
	public class Error
	{
		public int Code { get; set; }
		public string Message { get; set; }
	}
}

