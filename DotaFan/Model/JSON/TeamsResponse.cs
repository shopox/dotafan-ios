﻿using System;
using System.Collections.Generic;

namespace DotaFan.Model.JSON
{
	public class TeamsResponse
	{
		public List<Team> Teams { get; set; }
	}
}

