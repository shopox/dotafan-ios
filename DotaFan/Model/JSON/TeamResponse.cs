﻿using System;

namespace DotaFan.Model.JSON
{
	public class TeamResponse
	{
		public Team Team { get; set; }
		public bool IsFollowing { get; set; }
	}
}

