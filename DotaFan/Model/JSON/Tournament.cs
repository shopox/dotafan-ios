﻿using System;

namespace DotaFan.Model.JSON
{
	public class Tournament
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}

