﻿using System;

namespace DotaFan.Model.JSON
{
	public class MatchResponse
	{
		public Match Match { get; set; }
		public bool IsFollowing { get; set; }

		public MatchResponse()
		{
			IsFollowing = false;
		}
	}
}

