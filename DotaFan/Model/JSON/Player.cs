﻿using System;

namespace DotaFan.Model.JSON
{
	public class Player
	{
		public string Name { get; set; }
		public string Nickname { get; set; }
		public string Country { get; set; }
		public string Role { get; set; }

        public string GetRoleName()
        {
            return Role.tr();
        }

        public string GetCountryName()
        {
            return Flag.GetNameForCountry(Country);
        }
	}
}

