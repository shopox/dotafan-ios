﻿using System;

namespace DotaFan
{
	public class MyLogger
	{
		public MyLogger ()
		{
		}

		public static void Log(string message)
		{
			Console.WriteLine (message);
		}

		public static void Log(Exception e)
		{
			var message = String.Format ("Exception: {0}", e.Message);
			Log (message);
		}
	}
}

