﻿using System;
using MonoTouch.Foundation;

namespace DotaFan
{
    public static class Localizable
    {
        public static string tr(this string key)
        {
            return NSBundle.MainBundle.LocalizedString(key, "");
        }

        public static string tr_country(this string key)
        {
            return NSBundle.MainBundle.LocalizedString(key, "", "Country");
        }
    }
}

