﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using ModernHttpClient;

namespace DotaFan
{
	public static class HTTP
	{
        private static HttpClient client;

		public static TimeSpan Timeout 
        { 
			get
            {
                return client.Timeout;
            }
			set
            {
                client.Timeout = value;
            }
		}

		static HTTP()
		{
            client = new HttpClient(new NativeMessageHandler());
            Timeout = TimeSpan.FromSeconds (10);
		}

		public static string ToQueryString(Dictionary<string, object> parameters)
		{
			var items = parameters.Keys.Select(
				key => String.Format("{0}={1}", key, parameters[key].ToString())).ToArray();

			return String.Join("&", items);
		}

		public static Task<string> Get(string baseAddress, string url = "", Dictionary<string, object> parameters = null)
		{
            NetworkActivity.Show();
            var uri = new Uri (baseAddress, UriKind.Absolute);
            var uriBuilder = new UriBuilder(uri);
            uriBuilder.Path = url;

			if (parameters != null) 
                uriBuilder.Query = ToQueryString (parameters);

            var response = client.GetStringAsync(uriBuilder.Uri).ContinueWith(t =>
            {
                NetworkActivity.Hide();
                return t.Result;
            });
            return response;
        }

        public static async Task<Stream> DownloadAsync(string baseAddress, string url = "")
        {
            var requestUri = new Uri(new Uri(baseAddress), url);

            using (var request = new HttpRequestMessage(HttpMethod.Get, requestUri))
            {
                using (var response = await client.SendAsync(request))
                {
                    if (!response.IsSuccessStatusCode)
                        return null;

                    return await response.Content.ReadAsStreamAsync();
                }
            }
        }
	}
}

