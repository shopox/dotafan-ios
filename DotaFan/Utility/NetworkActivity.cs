﻿using System;
using MonoTouch.UIKit;

namespace DotaFan
{
    public static class NetworkActivity
    {
        public static void Show()
        {
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
        }

        public static void Hide()
        {
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
        }
    }
}

