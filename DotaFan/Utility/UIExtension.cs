﻿using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;
using System.Collections;
using System.Collections.Generic;

using MonoTouch.CoreGraphics;

namespace DotaFan
{
    public static class UIExtension
    {
        public static void SetHeight(this UIView view, float height)
        {
            RectangleF r = view.Frame;
            r.Height = height;
            view.Frame = r;
        }

        public static void SetWidth(this UIView view, float width)
        {
            RectangleF r = view.Frame;
            r.Width = width;
            view.Frame = r;
        }

        public static void SetLeft(this UIView view, float left)
        {
            RectangleF r = view.Frame;
            r.X = left;
            view.Frame = r;
        }

        public static void SetLocation(this UIView view, PointF p)
        {
            RectangleF r = view.Frame;
            r.X = p.X;
            r.Y = p.Y;
            view.Frame = r;
        }

        public static void SetLocation(this UIView view, float x, float y)
        {
            RectangleF r = view.Frame;
            r.X = x;
            r.Y = y;
            view.Frame = r;
        }

        public static void SetTop(this UIView view, float top)
        {
            RectangleF r = view.Frame;
            r.Y = top;
            view.Frame = r;
        }

        public static void SetSize(this UIView view, SizeF size)
        {
            RectangleF r = view.Frame;
            r.Size = size;
            view.Frame = r;
        }

        public static void SetSize(this UIView view, float width, float height)
        {
            view.SetSize(new SizeF(width, height));
        }
    }
}

